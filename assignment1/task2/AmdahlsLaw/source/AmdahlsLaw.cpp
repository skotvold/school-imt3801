/* Everything can be found @ 
   https://bitbucket.org/skotvold/school-imt3801/src/8426872b7be936e8a43448b3702946a5b9d188fc?at=master
*/

#include <iostream>
#include <chrono>
#include <windows.h>
#include <omp.h>
#include <array>
#include <fstream>

/* Code need to be compiled in c++11 
   and enable openMP, the code takes quite some time 
   to execute
*/

// testing purpose, should be 8 to be honest
const int MAXTHREADS = 16;

// simple typedefs to make code smaller and simpler to read
using myTimer = std::chrono::high_resolution_clock;
using ms = std::chrono::milliseconds;

// easier to read nested std::array
template<typename T,int l1,int l2>
using array2d = std::array<std::array<T,l1>,l2>;

// functions used in the program
void writeToCsv(array2d<int , MAXTHREADS , 5> result);
void doWork();



int main(int argc, char* argv[]){
	std::array < int, 5 > parallel_work = { 10, 30, 50, 70, 90 };
	array2d<int, MAXTHREADS, 5> resultTime;
	int pos = 0;
	
	std::cout << "Starting parallel work\n";
	for (int p : parallel_work){

		for (int i = 1; i <= MAXTHREADS; i++){
			// start time, set threads
			auto startTimer = myTimer::now();
			omp_set_num_threads(i);

			// serial code snippet
			#pragma omp single
			{
				for (int j = 1; j <= 100 - p; j++){
					doWork();
				}
			}

			// parallel code snippet
			#pragma omp parallel for
			for (int j = 1; j <= p; j++){
				doWork();
			}

			resultTime[pos][i - 1] = std::chrono::duration_cast<ms>(myTimer::now() - startTimer).count();

		}
		pos++;
		std::cout << "thread: " << p << "\n";
	}



	writeToCsv(resultTime);

	system("pause");
	return 0;
}


void doWork()
{	
	Sleep(50);
}

void writeToCsv(array2d<int, MAXTHREADS, 5> result)
{
	std::ofstream file("sample.csv");
	file << "threads(N)" << "," << "10%" << "," << "30%" << "," 
		 << "50%" << "," << "70%" << "," << "90%"<<"\n";

	int orig = result[0][0];
	for (int i = 0; i < MAXTHREADS; i++){
		std::array<double_t, 5> speedup;

		for (int j = 0; j < 5; j++){
			speedup[j] = ((double_t)orig / (double_t)result[j][i]);
		}
		
		printf("writing thread number %d\n", i+1);
		file << i + 1 << "," << speedup[0] << "," << speedup[1] << ","
			 << speedup[2] << "," << speedup[3] << "," << speedup[4] 
		 	 << "\n";

	}
	printf("\ndone");
	file.close();	

}

