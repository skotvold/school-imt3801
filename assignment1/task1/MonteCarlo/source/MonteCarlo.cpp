/* Everything can be found @
   https://bitbucket.org/skotvold/school-imt3801/src/8426872b7be936e8a43448b3702946a5b9d188fc?at=master
*/


#include <iostream>
#include <random>
#include <chrono>
#include <omp.h>

/* To run this program you need c++11  
   and enabled openMP
*/

// simple define for x*x and y*y calcuation
#define diff(x,y) x*x + y*y

// typedefs to make code easier to read
using myTimer = std::chrono::high_resolution_clock;
using ms = std::chrono::milliseconds;


int main(int argc, char* argv[]){
	// Random generator initalization
	std::random_device rd;		
	std::mt19937_64 gen(rd());
	std::uniform_real_distribution<double> dis(0.0 , 1.0);

	// number in's and max number used  
	uint64_t in = 0;
	uint64_t maxN = 1000000;

	// start timer and Monte Carlo's approximation of PI
	auto start = myTimer::now();
	#pragma omp parallel for reduction(+:in)
  //#pragma omp parallel for 
	for (int i = 1; i <= maxN; i++){
		auto x = dis(gen);
		auto y = dis(gen);
		
		if (diff(x, y) <= 1.0){
			//#pragma omp atomic
			//#pragma omp critical
			in += 1;
			
		}
	}
	
	// calcualte answer and time used
	double_t result = static_cast<double_t>(in * 4) / static_cast<double_t>(maxN);
	auto end = myTimer::now();
	auto runtime = std::chrono::duration_cast<ms>(end - start).count();
	
	// Print the answer
	std::cout << "Runtime of calculation: " << runtime << "ms = " 
			  << static_cast<double_t>(runtime)/static_cast<double>(1000) << " sec\n"
			  << "answer: (" << in <<" *  4) / " << maxN << " = " << result << "\n";
	

	system("pause");
	return 0;
}

/*		
Some Results  

	no multithreading
		1. maxN(10000000): 3.14155, 2929ms = 2.929 sec
		2. maxN(1000000): 3.14354, 285ms = 0.285 sec

	reduction: #pragma omp parallel for reduction(+:in) 
		1. maxN(1000000): 3.14264 , 086ms = 0.086sec 
		2. maxN(10000000): 3.14097, 908ms = 0.908sec

	atomic: #pragma omp atomic
		1. maxN(10000000): 3.14108, 992ms = 0.992sec
		2. maxN(10000000): 3.14109, 1048ms = 1.048sec
		3. maxN(1000000): 3.13994, 173ms = 0.173sec

	critical: #pragma omp critical
		1. maxN(10000000): 3.13197, 1673ms = 1.673sec
		2. maxN(10000000): 3.1402, 1666ms = 1.666sec
		3. maxN(1000000): 3.14077, 179ms = 0.179sec

*/