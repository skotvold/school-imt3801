#include <iostream>
#include <pthread.h>
#include <array>
#include <string>
#include "safeQueue.h"
#include <functional>


// global variables
safeQueue<std::string> g_queueWork;			// queue between inputThread and workThread
safeQueue<std::string> g_queuePrint;		// queue between workThread and outputThread
safeQueue<int> g_queueLen;					// queue to store string length between workthread, and outputThread
static bool g_quit{ false };				// global variable if we want to quit


class Callbacks{
public:
	std::string getKey_one()	{ return "one";		}
	std::string getKey_two()	{ return "two";		}
	std::string getKey_three()	{ return "three";	}
	std::string getKey_four()	{ return "four";	}
	std::string getKey_five()	{ return "five";	}

};

// Callback
typedef std::string(Callbacks::*funcptr)();

// function that get the callbacks
funcptr getCallback(int i){
	std::array<std::array<funcptr, 5>, 2> ptr;
	static int toggle = 1;

	toggle = (toggle == 0);

	// Callback handling (rofl)
	ptr[0][0] = &Callbacks::getKey_one;
	ptr[0][1] = &Callbacks::getKey_two;
	ptr[0][2] = &Callbacks::getKey_three;
	ptr[0][3] = &Callbacks::getKey_four;
	ptr[0][4] = &Callbacks::getKey_five;

	ptr[1][0] = &Callbacks::getKey_five;
	ptr[1][1] = &Callbacks::getKey_four;
	ptr[1][2] = &Callbacks::getKey_one;
	ptr[1][3] = &Callbacks::getKey_two;
	ptr[1][4] = &Callbacks::getKey_three;

	// return correct callback
	return ptr[toggle][i - 1];
}

class inputBehaviour{
private:
	std::string input;
public:
	inputBehaviour(){}

	// set the input for the user
	void setInput(std::string p_input){
		this->input = p_input;
	}

	// set the global input to false
	void setQuit(){
		g_quit = true;
	}

	// if 1-5 we do a single callback
	void singleCallback(){
		Callbacks* cb = new Callbacks;
		funcptr func = getCallback((int)(input[0] - '0'));
		auto t = (cb->*func)();
		g_queueWork.push(t);
	}

	// if space is pressed we do five callbacks
	void multipleCallbacks(){
		Callbacks* cb = new Callbacks;
		for (size_t i = 1; i <= 5; i++){
			funcptr func = getCallback(i);
			auto t = (cb->*func)();
			g_queueWork.push(t);
		}
	}

	// if "any" other key is pressed we do nothing
	void doNothing()	{ /*	Nothing	*/ }

};


void* inputThread(void* args){

	// temp input
	std::string input;

	// behave after input
	inputBehaviour ib;

	// array to contain the ascii values(keyboard values)
	std::array <std::function<void()>, 256 > handleArray;

	// assign input behaviour function
	std::function<void()> doQuit = std::bind(&inputBehaviour::setQuit, &ib);
	std::function<void()> inRange = std::bind(&inputBehaviour::singleCallback, &ib);
	std::function<void()> space = std::bind(&inputBehaviour::multipleCallbacks, &ib);
	std::function<void()> ignore = std::bind(&inputBehaviour::doNothing, &ib);



	// assign every value to ignore
	for (size_t i = 0; i < handleArray.size(); i++){
		handleArray[i] = ignore;
	}

	// assign specific behaviour
	handleArray[(int)'1'] = inRange;
	handleArray[(int)'2'] = inRange;
	handleArray[(int)'3'] = inRange;
	handleArray[(int)'4'] = inRange;
	handleArray[(int)'5'] = inRange;
	handleArray[(int)'q'] = doQuit;
	handleArray[(int)' '] = space;

	while (!g_quit){

		// get a string and set it
		std::getline(std::cin, input);
		ib.setInput(input);

		// do behave after input
		handleArray[(unsigned int)input[0]]();
	}

	printf("input function shut down...\n");
	return NULL;
}



void* workThread(void* args){
	std::string workString;
	while (!g_quit){

		if (g_queueWork.try_pop(workString)){
			g_queuePrint.push(workString);
			g_queueLen.push(workString.length());
		}
	}

	printf("work function shut down...\n");
	return NULL;
}



void* outputThread(void* args){
	std::string printString;
	int printStringLength;

	while (!g_quit){

		if (g_queuePrint.try_pop(printString)){
			g_queueLen.try_pop(printStringLength);
			printf("%s has length equal to: %d\n", printString.c_str(), printStringLength);
		}

	}


	printf("output function shut down...\n");
	return NULL;
}




int main(int argc, char* argv[]){
	std::array<pthread_t, 3> thr;

	// Create the threads
	pthread_create(&thr[0], NULL, inputThread, NULL);
	pthread_create(&thr[1], NULL, workThread, NULL);
	pthread_create(&thr[2], NULL, outputThread, NULL);

	for (auto& t : thr){
		pthread_join(t, NULL);
	}

	std::cout << "Press to finish...\n";
	std::cin.sync();
	std::cin.ignore();

	return 0;
}