#pragma once
#include <pthread.h>
#include <queue>

#ifdef _WIN32
	#pragma comment(lib,"pthreadVC2.lib")
#endif

/* Simple wrapper class for mutex */

class mutex {
	pthread_mutex_t m;
public:
	mutex(){
		pthread_mutex_init(&m, NULL);
	}

	void lock(){
		pthread_mutex_lock(&m);
	}

	void unlock(){
		pthread_mutex_unlock(&m);
	}

	pthread_mutex_t& getMutex(){
		return m;
	}
};


/* Simple lock class for the mutex */

class lock {
	mutex &m;
public:
	lock(mutex &m) : m(m){
		m.lock();
	}

	~lock() {
		m.unlock();
	}
};



template<typename T>
class safeQueue{
	std::queue<T>m_queue;
	mutex m_mutex;
	pthread_cond_t m_condv;

public:

	safeQueue(){
		pthread_cond_init(&m_condv, NULL);
	}
	void push(const T& item){
		lock l(m_mutex);
		m_queue.push(item);
		pthread_cond_broadcast(&m_condv);
	}

	bool try_pop(T& val){
		lock l(m_mutex);

		if (m_queue.empty()){
			return false;
		}

		val = m_queue.front();
		m_queue.pop();
		return true;

	}

	void waitAndPop(T& val){
		lock l(m_mutex);
	
		while (m_Queue.empty()){
			pthread_cond_wait(&m_condv, &m_mutex.getMutex());
		}

		val = m_queue.front();
		m_queue.pop();
	}

	T size() const {
		lock l(m_mutex);
		return m_queue.size();
	}

	void clear(){
		lock l(m_mutex);
		while (!m_queue.empty()){
			m_queue.pop();
		}
	}

};
