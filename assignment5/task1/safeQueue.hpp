#pragma once
#include <mutex>
#include <queue>
#include <condition_variable>

template<typename T>
class safeQueue{
	std::queue<T>m_queue;
	std::mutex m_mutex;
	std::condition_variable m_condv;

public:
	void push(const T& item){
		std::unique_lock<std::mutex> lock(m_mutex);
		m_queue.push(item);
		lock.unlock();
		m_condv.notify_one();
	}

	bool try_pop(T& val){
		std::unique_lock<std::mutex> lock(m_mutex);

		if (m_queue.empty()){
			return false;
		}

		val = m_queue.front();
		m_queue.pop();
		return true;

	}

	void waitAndPop(T& val){
		std::unique_lock<std::mutex> lock(m_mutex);
		m_condv.wait(lock,[&] { return !m_queue.empty(); });
		val = m_queue.front();
		m_queue.pop();
	}

	T size() const {
		std::unique_lock<std::mutex> lock(m_mutex);
		return m_queue.size();
	}

	void clear(){
		std::unique_lock<std::mutex> lock(m_mutex);
		while (!m_queue.empty()){
			m_queue.pop();
		}
	}

};