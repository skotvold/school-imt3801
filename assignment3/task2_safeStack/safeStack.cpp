#include "safeStack.hpp"
#include <iostream>

safeStack::safeStack(){
	pthread_cond_init(&m_cond, NULL);
}


safeStack::~safeStack(){
	clear();
	pthread_cond_destroy(&m_cond);
	pthread_mutex_destroy(&m_mutex.getMutex());
}


void safeStack::waitAndPop(int& val, int thread_id){
	lock l(m_mutex);
	while (m_stack.empty()){
		pthread_cond_wait(&m_cond, &m_mutex.getMutex());
	}

	val = m_stack.top();
	m_stack.pop();

	// printf for debug purpose
	printf("thread %d: pop = %d\n", thread_id, val);
}




bool safeStack::try_pop(int& val, int thread_id){
	lock l(m_mutex);
		
	if (m_stack.empty()){
		return false;
	}

	val = m_stack.top();
	m_stack.pop();

	// printf for debug purpose
	printf("thread %d: pop = %d\n", thread_id, val);
	return true;
}





void safeStack::push(const int& item, int thread_id){
	lock l(m_mutex);
	pthread_cond_broadcast(&m_cond);
	m_stack.push(item);

	// printf for debug, -1 means we don't want to print
	if (thread_id >= 0){
		printf("thread %d: push = %d\n", thread_id, item);
	}
}




int safeStack::size(){
	lock l(m_mutex);
	return m_stack.size();
}



void safeStack::clear(){
	lock l(m_mutex);
	
	while (!m_stack.empty()){
		m_stack.pop();
	}
}


void safeStack::printStack(){
	size_t end = size();
	for (size_t i = 1; i <= end; i++){
        int val = m_stack.top();
        printf("%d ", val);
		m_stack.pop();

		if (i % 10 == 0){
			printf("\n");
		}

	}
}
