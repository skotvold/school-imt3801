#include <iostream>
#include <array>
#include <thread>		// for sleep
#include <chrono>		// sleep

#include "safeStack.hpp"


/* Global stack array */
safeStack s;


// printf doesn't seem to be threadsafe on mingw32
#ifdef __MINGW32__
    mutex mu;
#endif

 struct Params{
    int id;
    Params(int p_id){
        id = p_id;
    }
 };



	/* Each thread will pop x elements each */
	void* testPop(void* args){
         if (Params* param = reinterpret_cast<Params*>(args)){
            int val;
            for (int i = 0; i < 5; i++){
                s.try_pop(val, param->id);

                std::this_thread::sleep_for(std::chrono::milliseconds(200));
            }

        }

       return NULL;

	}


	/* Each thread will push numbers to the stack */
	void* testPush(void* args){
          if (Params* param = reinterpret_cast<Params*>(args)){
            int start = param->id * 3;		// to get some number diversity
            int end = start + 5;	// iterates 5 times

            // push 5 elements to the stack
            for (int i = start; i < end; i++){
                s.push(i, param->id);

                // trying to make each thread wait a little
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        }

		return NULL;
	}

	void first_test(){
		std::array<pthread_t, 4> thr;
		printf("threads running: testPush\n");

		// Create the 4 threads to execute the test push
        int id = 0;
		for (pthread_t& t : thr){
            pthread_create(&t, NULL, testPush, new Params(id++));
		}

		// join the 4 threads to main thread
		for (pthread_t& t : thr){
			pthread_join(t, NULL);
		}


		printf("\nsize of stack: %d\n", s.size());
		printf("thread in reverse order of the push\n");
		s.printStack();
		s.clear();
	}

	void second_test(){
		std::array<pthread_t, 4> thr;	// threads

		// Push some elements(0 - 19) to the stack
		for (int i = 0; i < 20; i++){
			s.push(i);
		}

		printf("threads running: testPop\n");
		printf("Should count from 19 - 0 in order\n");

		// Create the 4 threads to execute the test push
        int id = 0;
		for (pthread_t& t : thr){
            pthread_create(&t, NULL, testPop, new Params(id++));
		}


		// join the 4 threads to main thread
		for (pthread_t& t : thr){
			pthread_join(t, NULL);
		}
		printf("The pop test was done!\n");
	}

	void third_test(){
		std::array<pthread_t, 4> thr;	// threads


		printf("threads running: testPop and testPush\n");

		// Create the 4 threads to execute pop and push paralell
        int id = 0;
        pthread_create(&thr[0], NULL, testPush, new Params(id++));
        pthread_create(&thr[1], NULL, testPop,  new Params(id++));
        pthread_create(&thr[2], NULL, testPop,  new Params(id++));
        pthread_create(&thr[3], NULL, testPush, new Params(id++));

		// join the 4 threads to main thread
		for (pthread_t& t : thr){
			pthread_join(t, NULL);
		}


		printf("testing of the stack is finished!\n");
	}


int main(){
	printf("running first test!\n");
	first_test();

	std::cout << "Press <enter> to continue..." << std::endl;
	std::cin.clear();
	std::cin.ignore();

	printf("\n----------------------------\n\n");
	printf("Running second test!\n");
	second_test();

	std::cout << "Press <enter> to continue..." << std::endl;
	std::cin.clear();
	std::cin.ignore();

	printf("\n----------------------------\n\n");
	printf("Running third test!\n");
	third_test();

	std::cout << "Press <enter> to continue..." << std::endl;
	std::cin.clear();
	std::cin.ignore();

	pthread_exit(NULL);
	return 0;
}
