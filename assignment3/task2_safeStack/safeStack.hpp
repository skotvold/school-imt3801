#pragma once
#include <pthread.h>
#include <stack>

#ifdef _WIN32
    #pragma comment(lib,"pthreadVC2.lib")
#endif

	/* Simple wrapper class for mutex */

class mutex {
	pthread_mutex_t m;
public:
    mutex(){	
		pthread_mutex_init(&m,NULL);
	}

    void lock(){
		pthread_mutex_lock(&m);
	}
 
	void unlock(){
		pthread_mutex_unlock(&m);
	}
	
	pthread_mutex_t& getMutex(){
		return m;					 
	}
};


	/* Simple lock class for the mutex */

class lock {
	mutex &m;
public:
	lock(mutex &m) : m(m){
		m.lock();
	}
	
	~lock() { 
		m.unlock(); 
	}
};

		/* threadsafe stack */

// template<typename T> [Not used]
class safeStack {

	std::stack<int> m_stack;	// the stack
	mutex m_mutex;				// mutex
	pthread_cond_t m_cond;		// condition variable to signal

public:

	/* Constructor */
	safeStack();

	/* Destructor */
	~safeStack();
	
	/*
	 *  - waitAndPop()
	 *  - pop an element in the stack if it is not empty
	 *    If the stack is empty, the thread executing the pop
	 *    Will go into wait mode. 
	 *  - thread_id is used for testing and debugging, should be removed!
	 */
	 
	void waitAndPop(int& val, int thread_id);

	/*
	 * - try_pop()
	 * - attempts to pop an element, if the stack is empty
	 *   return false, if the stack successful popped the element
	 *   the function return true
	 * - thread_id is used for testing and debugging, should be removed!
	 */
	
	bool try_pop(int& val, int thread_id);
	

	/*
	 * - push()
	 * - push an element to the stack
	 * - thread_id is used for testing and debugging, should be removed!
	 *   -1 if no printing
	 */

	void push(const int& item, int thread_id = -1);

	/*
	 * - size()
	 * - get the size of the stack
	 */

	int size();

	/*
	 * - clear()
	 * - clear the stack
	 */

	void clear();

	/*
	 * - printStack()
	 * - Print and clear the stack, used for testing [not threadsafe]
	 */
	
	void printStack();

};
