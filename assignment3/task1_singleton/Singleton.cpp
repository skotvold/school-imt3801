#include "Singleton.hpp"

// initalize private static variables
mutex Singleton::m_mutex;
Singleton* Singleton::instance = NULL;


Singleton::Singleton(){
	#ifdef _DEBUG
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	#endif
}

Singleton* Singleton::getInstance(){
	lock l(m_mutex);

	if (instance == 0)
		instance = new Singleton;

	return instance;

}



