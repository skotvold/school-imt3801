#pragma once
#include <pthread.h>	
#include <thread>		// just for sleep
#include <chrono>		// just for sleep


#ifdef _WIN32
	
	#pragma comment(lib,"pthreadVC2.lib")
#endif

/* Simple wrapper class for mutex */
class mutex {
	pthread_mutex_t m;
public:
	mutex(){	
		pthread_mutex_init(&m, NULL);	
	}

	void lock()   {
		pthread_mutex_lock(&m);
	}
	
	void unlock() {	
		pthread_mutex_unlock(&m);
	}
};


/* Simple lock class for the mutex */
class lock {
	mutex &m;
public:
	lock(mutex &m) : m(m){ 
		m.lock(); 
	}

	~lock() { 
		m.unlock(); 
	}
};


/* Simple singleton */
class Singleton {
public:
	
	/*
	 * - getInstance()
	 * - get the singleton instance.
	 * - return Singleton*
	 */
	
	static Singleton* getInstance();
	
protected:

	/* Constructor */
	Singleton();

private:
	static mutex m_mutex;		// mutex to lock the instance
	static Singleton* instance;	// Singleton instance
};


