#include <iostream>
#include <array>				
#include "Singleton.hpp"

struct Params{
	int id;
	Params(int p_id){
		id = p_id;
	}
};

/* Test if singleton has same instance */
void* testFunction(void* args){
	if (Params* param = reinterpret_cast<Params*>(args)){
		printf("thread: %d = %p\n", param->id, Singleton::getInstance());
		return NULL;
	}
}


int main(int argc, char* argv[]){
	std::array<pthread_t, 4> my_threads;
	printf("Test singleton:\n");
	printf("threadID x = instance p\n");
	
	// Create the threads
	int id = 0;	
	for (auto& t : my_threads){
		pthread_create(&t, NULL, testFunction, new Params{id++});
		
		#ifdef _DEBUG
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		#endif
	}


	// Join the threads
	for (auto& t : my_threads){
		pthread_join(t, NULL);
	}

	// pause
	std::cout << "Press <enter> to continue..." << std::endl;
	std::cin.clear();
	std::cin.ignore();

	pthread_exit(NULL);
	return 0;
}

