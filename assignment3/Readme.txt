Problems log:

1)
problem: I had problem with pthreads at the start.

answer:  I solve this by first implementing in c++11, then remake
	 the code for pthreads, I made a mutex class and locker class
	 to wrap the pthreads mutex and locking to make it look and
	 feel more like c++.

2)
problem: the pop function use the while(empty) cond_wait
         approach, this approach is nice for consumer/producer i guess
         or when you have a way to shutdown the threads by input. if not
	 the consumer thread if there is nothing more to push, but he still want to pop
	 he will wait and never shut down the thread.

answer   I found a simple solution to add two different pops() a try_pop() and the waitAndPop
	 that i explained in the problem. try_pop will return false or true if he managed to pop,
	 and pop the element, that means if it have remaining pops in a loop he will just return false
         you can also break the loop when it is done.

3)
problem: Had a problem with the printing to make it sync, sometimes it seemed the stack was printing wrong order
	 , but the print in the stack class was correct, so i figured out it was just that the print wasn't insync.

answer: I just sent the id of the stack into the safestack and let the pop and push methods print what they was doing
	this should be removed of course if im going to actually use the stack. 