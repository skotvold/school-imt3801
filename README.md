# README #


### What is this repository for? ###

* Assignments for IMT3801 -  multi-threading.

### How do I get set up? ###

* Visual studio is not required, but would make things easier for you
* All the code is compiled with visual studio 2013 and miniGw used with qt_creator Ide
* You will need c++11
* All code is compiled for windows, but should be fairly simple to change
	ex:  Sleep(100) <-- windows.h