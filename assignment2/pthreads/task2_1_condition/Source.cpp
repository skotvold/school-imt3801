#include <pthread.h>
#include <iostream>
#include <regex>
#include <string>
/*
 * BAD SOLUTION
 */

#ifdef _WIN32
	#pragma comment(lib,"pthreadVC2.lib")
#endif



struct Params{
	pthread_mutex_t mutex_var[2];
	pthread_cond_t conditon_var[2];
	int shared_var1,shared_var2;
	bool program_done;

	Params(){
		program_done = false;
		shared_var1 = 0;
		shared_var2 = 0;

		mutex_var[0] = PTHREAD_MUTEX_INITIALIZER;
		mutex_var[1] = PTHREAD_MUTEX_INITIALIZER;
		conditon_var[0] = PTHREAD_COND_INITIALIZER;
		conditon_var[1] = PTHREAD_COND_INITIALIZER;
	}
};

void* inFunc  (void* args);
void* workFunc(void* args);
void* outFunc (void* args);
std::string str;

int main(int argc, char* argv[]){
	pthread_t handler[3];
	Params params;



	pthread_create(&handler[0], NULL, inFunc, &params);
	pthread_create(&handler[1], NULL, workFunc, &params);
	pthread_create(&handler[2], NULL, outFunc, &params);
	
	for (size_t i = 0; i < 3; i++){
		pthread_join(handler[i], NULL);
	}
	
	system("pause");
	pthread_exit(NULL);
}

void* inFunc(void* args){
	Params* params = (Params*)args;
	std::regex number("-?[[:digit:]]+"); 

	while(!params->program_done){
		std::cin >> str;
	
		if (std::regex_match(str, number) && str[0] != '0'){
			int num = std::stoi(str);
			
			pthread_mutex_lock(&params->mutex_var[0]);
			pthread_cond_wait(&params->conditon_var[0], &params->mutex_var[0]);
			params->shared_var1 = num;
			printf("inFunc sending %d\n", num);
			pthread_mutex_unlock(&params->mutex_var[0]);
		
		}
		
		else if (str == "q" && str.length() == 1){
			params->program_done = true;
			pthread_cond_signal(&params->conditon_var[0]);
			pthread_cond_signal(&params->conditon_var[1]);
		}

	}

	printf("inFunc quitting...");
	return NULL;
	
}

void* workFunc(void* args){
	Params* params = (Params*)args;
	while (!params->program_done){
		
		
		pthread_mutex_lock(&params->mutex_var[0]);
		if (params->shared_var2 == params->shared_var1 * 3){
			pthread_cond_signal(&params->conditon_var[0]);

		}else{
			
			pthread_mutex_lock(&params->mutex_var[1]);
			params->shared_var2 = params->shared_var1 * 3;
			printf("workFunc turning %d into %d", params->shared_var1, params->shared_var2);
		
			pthread_cond_wait(&params->conditon_var[1], &params->mutex_var[1]);
			pthread_mutex_unlock(&params->mutex_var[1]);
			params->shared_var1 = 0;	
		}
		pthread_mutex_unlock(&params->mutex_var[0]);
	}


	printf("\nworkFunc quitting...");
	return NULL;
}

void* outFunc(void* args){
	Params* params = (Params*)args;
	
	while (!params->program_done){

		pthread_mutex_lock(&params->mutex_var[1]);
		if (params->shared_var2 != 0){
			printf("outFunc printing: %d\n", params->shared_var2);
			params->shared_var2 = 0;

		}else{
			pthread_cond_signal(&params->conditon_var[1]);
		}
		pthread_mutex_unlock(&params->mutex_var[1]);
	}

	printf("\noutFunc quitting...\n");
	return NULL;
}