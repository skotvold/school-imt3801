#include <pthread.h>		// pthreads
#include <semaphore.h>		// pthreads
#include <iostream>			// C++
#include <string>			// C++
#include <regex>			// C++11

#ifdef _WIN32
	#pragma comment(lib,"pthreadVC2")
#endif

struct Params{
	sem_t sem_var;
	int shared_var1;
	int shared_var2;
	bool finished;
	
	Params(){
		shared_var1 = 0;
		shared_var2 = 0;
		finished = false;
	}
};

void* inFunc(void* args);
void* workFunc(void* args);
void* outFunc(void* args);
bool isInt(const std::string &s);		// check if a number is integer


int main(){
	pthread_t handler[3];
	Params params;
	sem_init(&params.sem_var, 0, 0);
	
	// Create three threads
	pthread_create(&handler[0], NULL, inFunc, &params);
	pthread_create(&handler[1], NULL, workFunc, &params);
	pthread_create(&handler[2], NULL, outFunc, &params);

	// main thread wait for the others thread to finish and join up
	for (int i = 0; i < 3; i++){
		pthread_join(handler[i], NULL);
	}

	sem_destroy(&params.sem_var);
	#ifdef _WIN32
		system("pause");
	#endif
	pthread_exit(NULL);
}

void* inFunc(void* args){
	Params* params = (Params*)args;
	std::string input;
//	std::regex number("-?[[:digit:]]+"); // check if the string is a number or not
	
	while(!params->finished){	
		std::cin >> input;
		if (isInt(input) && input[0] != '0'){
		//if (std::regex_match(input, number) && input[0] != '0'){
			params->shared_var1 = std::stoi(input);
			printf("inFunc sending %d\n", params->shared_var1);
			sem_post(&params->sem_var);
		}
	
		else if (input[0] == 'q' && input.length() == 1) {
			params->finished = true;	// end the loop
			sem_post(&params->sem_var); // send exit to workFunc
			sem_post(&params->sem_var); // send exit to outFunc
		}
	}

	printf("inFunc quitting...");
	return NULL;
}

void* workFunc(void* args){
	Params* params = (Params*)args;
	
	while (!params->finished){
		sem_wait(&params->sem_var);
		
		if (params->finished)
			continue;

		// Work func multiply
		params->shared_var2 = params->shared_var1 * 3;
		printf("workFunc turning %d into %d \n", params->shared_var1, params->shared_var2);
		sem_post(&params->sem_var);
	}

	printf("\nworkFunc quitting...");
	return NULL;
}

void* outFunc(void* args){
	Params* params = (Params*)args;
	
	while(!params->finished){		
		sem_wait(&params->sem_var);
		
		if (params->finished) 
			continue;
		
		printf("outFunc printing: %d\n", params->shared_var2);
	}

	printf("\noutFunc quitting...\n");
	return NULL;
}

bool isInt(const std::string &s){
	if (s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false;
	char* p;
	strtol(s.c_str(), &p, 10);
	return (*p == 0);
}