#include <iostream>
#include <pthread.h>


/*
 * Another approach to the task
 * less c++11 and more c
 */
#ifdef _WIN32
	#pragma comment(lib,"pthreadVC2.lib")
#endif

float randNumGen();
void* doCalc(void* args);

void* g_hits;
const int N = 4;
const int MAX = 100000000;
int tot_iter;


int main(){
	tot_iter = MAX / N;
	pthread_t  handler[N];
	
	for (int i = 0; i < N; i++){
		pthread_create(&handler[i], NULL, doCalc, NULL);
	}

	int totalHits = 0;
	for (int i = 0; i < N; i++){
		pthread_join(handler[i], &g_hits);
		totalHits += *(int*)g_hits;
	}

	double pi = 4*(totalHits/(double)MAX);
	printf("PI = %f\n", pi);

	system("pause");
	pthread_exit(NULL);
}

float randNumGen(){
	int random_value = rand();							// Generate a random number   
	float unit_random = random_value / (float)RAND_MAX; // make it between 0 and 1 
	return unit_random;
}

void* doCalc(void* args){
	int* m_hits = (int*)calloc(1, sizeof(int));

	for (int i = 0; i < tot_iter; i++){
		float x = randNumGen();
		float y = randNumGen();

		if (x*x + y*y <= 1.0){
			++(*m_hits);
		}
	}

	return (m_hits);
}