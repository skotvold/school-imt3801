/*
 * Code can be found @ https://bitbucket.org/skotvold/school-imt3801/
 * You need c++11 to run this code
 */

#include <iostream>		// c++
#include <chrono>		// c++11
#include <random>		// c++11
#include <pthread.h>	// pthreads
#include <thread>
#include <mutex>
#include <array>
#include <utility>
#include <atomic>
#include <condition_variable>

#ifdef _WIN32
	#pragma comment(lib,"pthreadVC2.lib")
#endif

// for time clock(c++11)
typedef std::chrono::high_resolution_clock myTimer;	// define myTimer as keyword
typedef std::chrono::milliseconds ms;				// define ms as keyword

using std::ref;
/*
struct Data {
	unsigned long pi;
	Data() : pi(0){};
};


const size_t N = 4;					// Number of threads
const long MAX = 10000000;			// total number of points

int tot_iter;						// number of points per thread
Data data[N];						// Structure array to store data

void* doCalc(void* threadid);		// calculation function


int main(int argc, char* argv[]){
	pthread_t handles[N];
	tot_iter = (MAX / N);
	
	// start the threads
	auto start_time = myTimer::now();	
	for (size_t i = 0; i < N; i++){
		pthread_create(&handles[i],NULL, doCalc,&data[i]);
	}

	// join the threads
	for (size_t i = 0; i < N; i++){
		pthread_join(handles[i] , NULL);
	}

	// add the result together
	double pi = 0.0;
	for (size_t i = 0; i < N; i++){
		pi += (double)4.0 * ((double)data[i].pi / (double)MAX);
	}

	auto runtime = std::chrono::duration_cast<ms>(myTimer::now() - start_time).count();
	printf("Result %f\nTime: %ld ms\n", pi, runtime);


#ifdef _WIN32
	system("pause");
#endif
	pthread_exit(NULL);
}



// Calculate the the Monte Carlo approximation of pi
void *doCalc(void* threadid){
	if (Data* data = reinterpret_cast<Data*>(threadid)){
		for (int i = 0; i < tot_iter; i++){
			auto x = dis(gen);
			auto y = dis(gen);

			if (((x*x) + (y*y)) <= 1.0){
				data->pi++;
			}
		}
	}

	return data;
}

*/

struct Param {
	std::atomic<int> number;
	size_t reduce_iter;

} _params ;
//
//pthread_mutex_t  m= PTHREAD_MUTEX_INITIALIZER;


class calculation{
public:
	calculation(){}
	static void calc(std::atomic<int> &number, size_t reduce_iter){
		for (size_t i = 0; i < reduce_iter; i++){
			
			number++;
	
			
		}
	}

	static void* pcalc(void* args){
		Param* p = (Param*)args;

		for (size_t i = 0; i < p->reduce_iter; i++){
		/*	pthread_mutex_lock(&m);
			p->number++;
			pthread_mutex_unlock(&m);*/
		}

		return p;
	}
};


void* calc(void* args){
	Param* p = (Param*) args;

	for (size_t i = 0; i < p->reduce_iter; i++){
		p->number++;
	
	}

	return p;
}


const int num_threads = 100;
const int MAX = 1000000;
int main() {

	/*std::array<std::thread,  num_threads> threads{};
	std::atomic<int> number{ 0 };
	size_t iter = MAX / threads.size();
	calculation c;*/

	
	std::array<pthread_t, num_threads> threads{};
	_params.reduce_iter = MAX / threads.size();
	_params.number = 0 ;

	auto start_time = myTimer::now();
	for (auto &thr : threads){
		
		pthread_create(&thr, NULL, &calc, &_params);
		//thr = std::move( std::thread( &calculation::calc,&c, ref(number), ref(iter) ) );
	/*	thr = std::thread(&calculation::calc, ref(number), ref(iter));*/
	}

	for (auto& thr : threads){
	/*	if (thr.joinable()){
			thr.join();
		}*/

		pthread_join(thr, NULL);
	}

	auto runtime = std::chrono::duration_cast<ms>(myTimer::now() - start_time).count();

	printf("Result %d\nTime: %d ms\n", _params.number, runtime);
	/*printf("Result %d\nTime: %d ms\n", number, runtime);*/
	std::cout << "press to finish...\n";
	std::cin.sync();
	std::cin.get();
	return 0;
}