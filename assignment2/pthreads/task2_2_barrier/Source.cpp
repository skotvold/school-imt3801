#ifdef _WIN32
	#include <windows.h>	// windows | Sleep
#endif

#include <pthread.h>	// pthread
#include <string>		// C++
#include <iostream>		// C++
#include <regex>		// C++11

#ifdef _WIN32
	#pragma comment(lib,"pthreadVC2")
#endif

struct Params{
	pthread_barrier_t barrier_var;
	int shared_input;
	int shared_output;
	bool finished;

	
	Params(){
		shared_input  = 0;
		shared_output = 0;
		finished = false;
	}
};

void* inFunc(void* args);
void* workFunc(void* args);
void* outFunc(void* args);
bool isInt(const std::string &s);		// check if a number is integer


int main(int argc, char* argv[]){
	pthread_t handler[3];
	Params params;
	pthread_barrier_init(&params.barrier_var, NULL, 3);


	pthread_create(&handler[0], NULL, inFunc, &params);
	pthread_create(&handler[1], NULL, workFunc, &params);
	pthread_create(&handler[2], NULL, outFunc, &params);


	for (int i = 0; i < 3; i++){
		pthread_join(handler[i], NULL);
		pthread_barrier_destroy(&params.barrier_var);
	}

	system("pause");
	pthread_exit(NULL);
}

void* inFunc(void* args){
	Params* params = (Params*)args;
	std::string input;


	while (!params->finished){
		std::cin >> input;
		if (isInt(input) && input[0] != '0'){
			pthread_barrier_wait(&params->barrier_var);
			params->shared_input = std::stoi(input);
			printf("inFunc sending %d\n", params->shared_input);
		}
		else if (input[0] == 'q' && input.length() == 1){
			params->finished = true;
		}

	}

	// need to destroy the barrier lock
	pthread_barrier_wait(&params->barrier_var);
	printf("inFunc() quitting...\n");
	return NULL;
}

void* workFunc(void* args){
	Params* params = (Params*)args;
	while (!params->finished){
		pthread_barrier_wait(&params->barrier_var);
	
		// Make sure the loop exit
		if (params->finished)
			continue;
		
		params->shared_output = params->shared_input * 3;
		printf("workFunc turning %d into %d \n", params->shared_input, params->shared_output);

	}

	printf("doWork() quitting...\n");
	return NULL;
}

void* outFunc(void* args){
	Params* params = (Params*)args;
	while (!params->finished){
		pthread_barrier_wait(&params->barrier_var);
		
		// make sure the code exit
		if (params->finished)
			continue;
		
		// sleep to sync the wait... not sure if this is a good approach
		// outFunc will print  before workFunc if we don't sleep
		// possible solution two different barriers for one for
		// input -> work and one for work -> outprint
		#ifdef _WIN32
			Sleep(10);
		#endif

		printf("outFunc printing: %d\n", params->shared_output);
	}


#ifdef _WIN32
	Sleep(10);
#endif

	printf("outFunc() quitting...\n");
	return NULL;

}

bool isInt(const std::string &s){
	if (s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false;
	char* p;
	strtol(s.c_str(), &p, 10);
	return (*p == 0);
}