/*
 * approximation of pi with monte carlo
 * used a atomic value
 */


#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <random>

const int MAX = 10000000;
std::atomic<long> g_hits = 0;
const int numThreads = 8;


void monte_carlo(std::mt19937_64 gen){
	size_t reduction = MAX / numThreads; 
	for (size_t i = 1; i <= reduction; i++){
		auto x = std::uniform_real_distribution<double>(0.0, 1.0)(gen);
		auto y = std::uniform_real_distribution<double>(0.0, 1.0)(gen);
		
		if ( (x*x + y*y) <= 1){
			++g_hits;
		}
	}
}


int main(int argc, char *argv[]){
	// Initialize random generator
	std::random_device rd;
	std::mt19937_64 gen(rd());

	// Create a vector of threads
	std::vector<std::thread> m_threads;

	for (size_t i = 0; i < numThreads; i++){
		m_threads.push_back(std::thread(monte_carlo, gen));
	}

	for (auto& t : m_threads){
		t.join();
	}
	
	double ans = static_cast<double>(g_hits * 4) / static_cast<double>(MAX);
	std::cout << "result: " << ans << std::endl;

#ifdef _WIN32
	system("pause");
#endif
	return 0;
}

