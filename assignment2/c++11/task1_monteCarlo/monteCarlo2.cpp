/* 
 * this is the version 2
 * Tried a little lambda and future stuff with async, a little slow...
 * But it worked
 



#include <iostream>
#include <thread>
#include <future>
#include <chrono>
#include <random>

const int MAX = 10000000;
int main(int argc, char *argv[]){
	std::future<long> hits = std::async(std::launch::async, [](){
		std::cout << std::this_thread::get_id() << std::endl;
		long n = 0;
		std::random_device rd;
		std::mt19937_64 gen(rd());
		for (size_t i = 1; i <= MAX/2; i++){
			auto x = std::uniform_real_distribution<double>(0.0, 1.0)(gen);
			auto y = std::uniform_real_distribution<double>(0.0, 1.0)(gen);
			
			if ((x*x + y*y) <= 1){
				++n;
			}
		}

		return n;
	});

	double ans = static_cast<double>(hits.get() * 4) / static_cast<double>(MAX);
	std::cout << "result: " << ans << std::endl;

	system("pause");
	return 0;
}

*/