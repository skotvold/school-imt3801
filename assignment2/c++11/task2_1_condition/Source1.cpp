#include <iostream>
#include <vector>
#include <string>
#include <regex>
#include <thread>
#include <mutex>
#include <condition_variable>

/*
 * Task is done with mutex and conditions
 * to solve the problem/task given.
 */


struct Param {
	std::mutex lock_print;				// lock for the print
	std::mutex lock_work;				// lock for work done
	std::condition_variable signal;		// signal

	bool done;							// if program is done
	bool signal_wait_work;				// wait for the input function
	bool signal_wait_output;			// wait for the doWork function
	int shared_input;					// shared variable between inFunc and doWork
	int shared_output;					// shared variable between doWork and outFunc

	Param(){
		done = false;
		signal_wait_work = false;
		signal_wait_output = false;
	}
};

void inFunc(Param  *param);				// Input
void doWork(Param  *param);				// Work
void outFunc(Param *param);				// Out
bool isInt(const std::string &s);		// check if a number is integer

int main(int argc, char* argv[]){	
	std::vector<std::thread> m_threads;
	Param param;

	// create threads
	m_threads.push_back(std::thread(inFunc,&param));
	m_threads.push_back(std::thread(doWork, &param));
	m_threads.push_back(std::thread(outFunc, &param));


	// Join threads
	for (auto& t : m_threads){
		t.join();
	}

#ifdef _WIN32
	system("pause");
#endif

	return 0;
}



void inFunc(Param *param){
	std::string input;
	//std::regex number("-?[[:digit:]]+"); // check if the string is a number or not

	while (!param->done){
		std::cin >> input;

		if (input[0] == 'q' && input.length() == 1){
			param->done = true;
		}

		//else if (std::regex_match(input,number) && input[0] != '0'){
		else if (isInt(input) && input[0] != '0'){
			std::unique_lock<std::mutex> locker(param->lock_work);
			param->shared_input = std::stoi(input);
			
			std::cout << "inFunc sending: " << param->shared_input << "\n";
		
			param->signal_wait_work = true;
			param->signal.notify_one();
		}
	}

	param->signal_wait_output = true;
	param->signal_wait_work = true;
	param->signal.notify_all();
	std::cout << "inFunc() quitting...\n";
}

void doWork(Param *param){
	while (!param->done){
		std::unique_lock<std::mutex> locker(param->lock_work);
		std::unique_lock<std::mutex> print(param->lock_print);
		param->signal.wait(locker, [param]{return param->signal_wait_work;});
			
		if (param->done){
			std::cout << "doWork() quitting...\n";
			continue; 
		}
			
		param->shared_output = param->shared_input * 3;
		std::cout << "doWork multiply "<<param->shared_input 
				  << " into " << param->shared_output << "\n";
			
		param->signal_wait_work = false;
		param->signal_wait_output = true;
		param->signal.notify_one();
	}
}

void outFunc(Param *param){
	while (!param->done){
		std::unique_lock<std::mutex> locker(param->lock_print);
		param->signal.wait(locker, [param]{return param->signal_wait_output; });
		
		if (param->done){ 
			std::cout << "outFunc() quitting...\n";
			continue; 
		}
			
		std::cout << "outFunc: " << param->shared_output << "\n";	
		param->signal_wait_output = false;
		param->signal.notify_one();
	}
}


bool isInt(const std::string &s){
	if (s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false;
	char* p;
	strtol(s.c_str(), &p, 10);
	return (*p == 0);
}