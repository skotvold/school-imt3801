#include <thread>
#include <vector>
#include <iostream>
#include <regex>

#include <mutex>
#include <condition_variable>


/*
	* Task id done with semaphores
	* basic simple semaphore class based on
	* http://stackoverflow.com/questions/4792449/c0x-has-no-semaphores-how-to-synchronize-threads
*/



class Semaphore{
private:
	std::mutex mtx;
	std::condition_variable cv;
	int count;

public:
	Semaphore(int p_count = 0) : count(p_count){}
	void notify(){
		std::unique_lock<std::mutex> lck(mtx);
		++count;
		cv.notify_one();
	}

	void wait(){
		std::unique_lock<std::mutex> lck(mtx);
		cv.wait(lck, [this]() { return count > 0; });
		count--;
	}
};

struct Param {
	Semaphore s_input;
	Semaphore s_output;
	
	bool done;
	int shared_input;
	int shared_output;

	Param(){ done = false; }
};

void inFunc(Param  *param);
void doWork(Param  *param);
void outFunc(Param *param);
bool isInt(const std::string &s);		// check if a number is integer



int main(int argc, char* argv){
	std::vector<std::thread> m_threads;
	Param param;

	// create thread
	m_threads.push_back(std::thread(inFunc, &param));
	m_threads.push_back(std::thread(doWork, &param));
	m_threads.push_back(std::thread(outFunc, &param));

	// Join the threads
	for (auto& t : m_threads){
		t.join();
	}

#ifdef _WIN32
	system("pause");
#endif

	return 0;
}

void inFunc(Param *param){
	std::string input;
//	std::regex number("-?[[:digit:]]+");

	while (!param->done){
		std::cin >> input;
		
		if (input[0] == 'q' && input.length() == 1){
			param->done = true;
			std::cout << "inFunc() quitting...\n";
			param->s_output.notify();
			param->s_input.notify();
		}

	//	else if (std::regex_match(input, number) && input[0] != '0'){
		else if (isInt(input) && input[0] != '0'){
			param->shared_input = (std::stoi(input));
			std::cout << "inFunc sending: " << input << "\n";
			param->s_input.notify();
			
		}
	}
}

void doWork(Param *param){
	while (!param->done){

		param->s_input.wait();
		if (param->done){ 
			std::cout << "doWork() quitting...\n";
			continue; 
		}

		param->shared_output = param->shared_input * 3;
		std::cout << "doWork multiply " << param->shared_input
				  << " into " << param->shared_output << "\n";
		
		param->s_output.notify();
	}
}

void outFunc(Param *param){
	while (!param->done){
		param->s_output.wait();
		
		if (param->done){ 
			std::cout << "outFunc() quitting...\n";
			continue; 
		}
		std::cout <<"outFunc: " <<param->shared_output << std::endl;
	}
}

bool isInt(const std::string &s){
	if (s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false;
	char* p;
	strtol(s.c_str(), &p, 10);
	return (*p == 0);
}