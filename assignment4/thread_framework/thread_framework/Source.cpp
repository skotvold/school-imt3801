#include <iostream>
#include <thread>
#include <array>
#include <algorithm>
#include <string>


#include "input.h"
#include "output.h"
#include "worker.h"
#include "safeQueue.h"


int main(){
	input in;
	worker work;
	output out; 
	bool running = true;

	std::array<std::thread, 3> thr;
	thr[0] = std::thread(&input::user_input, &in, std::ref(work), std::ref(running));
	thr[1] = std::thread(&worker::work, &work, std::ref(out), std::ref(running));
	thr[2] = std::thread(&output::outputData, &out, std::ref(running));

	for (auto& t : thr){
		t.join();
	}
	
#ifdef _WIN32
	system("pause");
#endif 
}