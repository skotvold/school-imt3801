#include "worker.h"
#include <omp.h>
worker::worker(){}
worker::~worker(){}

void worker::work(output& p_output, bool& running){
	int val;
	while (running){
		if (m_queue.try_pop(val)){
			int end = val;
			#pragma omp parallel for reduction(+:val)
			for (int i = 0; i < end ; i++){
				val++;
			}
			
			printf("worker multiply %d\n", val);
			p_output.getQueue().push(val);
		}
	}

	printf("class worker shutting down\n");
}


safeQueue<int>& worker::getQueue(){
	return m_queue;
}

