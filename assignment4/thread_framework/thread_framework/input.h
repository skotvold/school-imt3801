#pragma once
#include "safeQueue.h"
#include "worker.h"


class worker;

class input{
public:
	input();
	~input();
	void user_input(worker& work, bool& running);
	bool isInt(const std::string &s);

private:
	safeQueue<int> m_queue;
};

