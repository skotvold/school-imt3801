#pragma once
#include "safeQueue.h"
#include "worker.h"

class output{

private:
	safeQueue<int> m_queue;
public:
	output();
	~output();
	void outputData(bool& running);
	safeQueue<int>& getQueue();
};

