#include "input.h"

#include <iostream>
#include <string>

input::input(){}
input::~input(){}

void input::user_input(worker& p_worker, bool& running){
	std::string input;
	while (running){
		std::cin >> input;
	
		if (isInt(input) && input[0] != '0'){
			p_worker.getQueue().push(std::stoi(input));
		}
		else if (input[0] == 'q' && input.length() == 1){
			running = false;
		}
	}

	printf("class input shutting down\n");
}


bool input::isInt(const std::string &s){
	if (s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+'))) return false;
	char* p;
	strtol(s.c_str(), &p, 10);
	return (*p == 0);
}