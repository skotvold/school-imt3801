#pragma once
#include "safeQueue.h"
#include "output.h"

class output;

class worker{
private:
	safeQueue<int> m_queue;
public:
	worker();
	~worker();
	void work(output& out, bool& running);
	safeQueue<int>& getQueue();
};

