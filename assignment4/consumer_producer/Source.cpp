#include <iostream>
#include <thread>
#include <random>
#include <vector>
#include "safeQueue.hpp"
#include <atomic>


// some typedefs
using std::vector;
using std::thread;

class Consumer{
public:
	void operator()(safeQueue<int>& p_queue, std::atomic<int>& sum){
		int pop_value;
		while (p_queue.try_pop(pop_value)){
			sum += pop_value;
		}
	}
};

int main(){
	std::random_device rd;
	std::mt19937_64 gen(rd());
	std::uniform_int_distribution<int>dis(1, 100);

	vector<thread> thr;
	vector<int> number_list(255);
	
	
	safeQueue<int> queue;
	std::atomic<int> thread_sum = 0;
	int serial_sum = 0;


	for (auto& n : number_list){
		n = dis(gen);
		queue.push(n);
	}

	// create consumers
	for (size_t i = 0; i < 16; i++){
		thr.push_back(std::thread(Consumer(), std::ref(queue), std::ref(thread_sum)));
	}

	// add serial sum
	for (auto& n : number_list){
		serial_sum += n;
	}
	

	for (auto& t : thr){
		if (t.joinable()){
			t.join();
		}
	}

	printf("serial sum = %d\nthread sum = %d\n", serial_sum, thread_sum);

#ifdef _WIN32
	system("pause");
#endif
	return 0;
}